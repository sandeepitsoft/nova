==NovaTable===

-- Create New Directory widget/table inside app directory and place below files from Git Repos()

--table.component.html
--table.component.scss
--table.component.spec.ts
--table.component.ts
--table.directive.spec.ts
--table.directive.ts
--data-property-getter-pipe/data-property-getter.pipe.spec.ts
--data-property-getter-pipe/data-property-getter.pipe.ts



--under app.module.ts add following lines.

	1.  import { TableComponent } from './widget/table/table.component';
		import {DataPropertyGetterPipe} from './widget/table/data-property-getter-pipe/data-property-getter.pipe';
		import { TableDirective } from './widget/table/table.directive';
		import {MatPaginatorModule} from "@angular/material/paginator";
		import {MatSortModule} from "@angular/material/sort";
		import {MatIconModule} from "@angular/material/icon";
		import { DragDropModule } from '@angular/cdk/drag-drop';

	2. inside @NgModule({
		declarations: [  add 
  
			TableComponent,DataPropertyGetterPipe, TableDirective
	3. inside imports : [ add
	
			   , MatPaginatorModule
			   , MatSortModule
			   , MatIconModule
			   , DragDropModule
			
			(if not mentioned before one of them above)
-- under parent component where we wanted to call NovaTable

	1. Create two Const for JSON object  (one for DATA )
	export const ELEMENT_DATA= [
	  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
	  {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
	  {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
	  {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
	  {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
	  {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
	  {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
	  {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
	  {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
	  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
	]; 

	--one for Columns (this will have three items name , dataKey,isSortable-true/fase)
	export const columns_data=[
	  { name: 'position', dataKey: 'position',isSortable: true },
	  { name: 'name', dataKey: 'name',isSortable: true},
	  { name: 'weight', dataKey: 'weight' ,isSortable: true},
	  { name: 'symbol', dataKey: 'symbol' ,isSortable: true},
	];


	2. inside OnInit() create two array one for holding data and another for columns 
	
	
		 testdata=[];
		 testcolumns:any[];
		 
	3. inside  ngOnInit():  set data and columns to table.
	
		 this.testdata=ELEMENT_DATA;
		 this.testcolumns=columns_data;
		 
	4. inside parentcomponent.html page add following to use NovaPTable Component
	
			<nova-table
				[tableData]="testdata"
				[tableColumns]="testcolumns"
				[isPageable]="true"
				[isResizable]="true"
				[paginationSizes]="[5,10,15]"
				[defaultPageSize]="5"
				(selectedrow)="onrowSelected($event)">
			</nova-table>

		where [tableData] -- pass the Table Data in JSON format
			  [tableColumns] --pass the Table columns to display
			  [isPageable] -- true/false depends if paging required
			  [paginationSizes] -- pagination size that will display in page dropdown
			  [defaultPageSize]=2 -- in case page will set no of row displayed in grid
			  [isResizable]--true/false depends if column resizing required
			  (selectedrow)="onrowSelected($event) -- implement this event to catch row selected event emitted from novaptable component on rowselect event
													   this will return selected row as JSON object.so we can perform any action on parent component using JSON object.
		


	
  
  

 
