import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FrmSelectRefComponent } from './frm-select-ref.component';

describe('FrmSelectRefComponent', () => {
  let component: FrmSelectRefComponent;
  let fixture: ComponentFixture<FrmSelectRefComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FrmSelectRefComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FrmSelectRefComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
