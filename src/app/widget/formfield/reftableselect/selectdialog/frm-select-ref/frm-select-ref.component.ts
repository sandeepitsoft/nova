import {Component, Inject, OnInit} from '@angular/core';
import { MatDialogRef ,MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ComponentService } from 'src/app/widget/componentservice.service';
import { DialogService } from 'src/app/widget/dialog/dialog.service';
export const grdGenRefData_SOURCEDATA= [
  {   Value1 : 'Row1 Col1',   Value2 : 'Row1 Col2',   Value3 : 'Row1 Col3', },
  {   Value1 : 'Row2 Col1',   Value2 : 'Row2 Col2',   Value3 : 'Row2 Col3', },
  {   Value1 : 'Row3 Col1',   Value2 : 'Row3 Col2',   Value3 : 'Row3 Col3', },
  {   Value1 : 'Row4 Col1',   Value2 : 'Row4 Col2',   Value3 : 'Row4 Col3', },
  {   Value1 : 'Row5 Col1',   Value2 : 'Row5 Col2',   Value3 : 'Row5 Col3', },
  {   Value1 : 'Row6 Col1',   Value2 : 'Row6 Col2',   Value3 : 'Row6 Col3', },
  {   Value1 : 'Row7 Col1',   Value2 : 'Row7 Col2',   Value3 : 'Row7 Col3', },
  {   Value1 : 'Row8 Col1',   Value2 : 'Row8 Col2',   Value3 : 'Row8 Col3', },
  {   Value1 : 'Row9 Col1',   Value2 : 'Row9 Col2',   Value3 : 'Row9 Col3', },
  {   Value1 : 'Row10 Col1',   Value2 : 'Row10 Col2',  Value3 : 'Row10 Col3',},
]; 
export const grdGenRefData_columns_data=[
  { name: 'code', headertext: 'Code', dataKey: 'code', isSortable: true },
  { name: 'description', headertext: 'Description', dataKey: 'description', isSortable: true }
  //,  { name: 'Col33', headertext: 'Col3', dataKey: 'Value3', isSortable: true },
];
@Component({
  selector: 'frm-select-ref',
  templateUrl: './frm-select-ref.component.html',
  styleUrls: ['./frm-select-ref.component.css']
})
export class FrmSelectRefComponent implements OnInit {
  isOpenedAsDialog : boolean = false;
  grdGenRefData_data:any;
  grdGenRefData_columns:any[];
  grdGenRefData_selectedRow = 0;
  isloading: boolean=true;
  filter="";
  selectedrow:any;
  constructor( private  dialogservice: DialogService
    , public dialogref:MatDialogRef<FrmSelectRefComponent>
    , private componentservice:ComponentService
    , @Inject(MAT_DIALOG_DATA) public inputdata:any
    
  ) {
      if(inputdata.data.data){
        this.filter=inputdata.data.data.table;
      }
    }

  ngOnInit(): void {
    this.inputdata.data.data.getList$.subscribe(data =>{
      this.isloading=false;
      this.grdGenRefData_data = data;
     }, error => this.isloading = false);
   
     // this.grdGenRefData_data=grdGenRefData_SOURCEDATA;
    this.grdGenRefData_columns=grdGenRefData_columns_data;
  }

  onClose() {
    if (this.isOpenedAsDialog)
    {
        this.dialogref.close();
    }
    
  }
  onrowSelected(row){
      this.selectedrow=row;
  }
   
  selectrow(){
    //alert(this.selectedrow);
    
    this.dialogref.close(this.selectedrow);
 }
}
