import { Component, Directive, ElementRef, forwardRef, HostListener, Input, OnInit, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { DialogService } from '../../dialog/dialog.service';
import { FrmSelectRefComponent } from './selectdialog/frm-select-ref/frm-select-ref.component';
import { ComponentService } from "../../componentservice.service";
import { tap } from "rxjs/operators";

@Directive({
  selector: '[refSearchInput]'
})
export class RefSearchInput implements OnInit {
  constructor(public parent: RefTableSelectComponent, public el: ElementRef) {
    this.parent.valuechange$.subscribe((a: any) => {
      if (a) {
        this.el.nativeElement.value = a.code;
      }
    })

  }

  @HostListener('blur') onBlur() {
    this.parent.writeValue(this.el.nativeElement.value);
  }

  ngOnInit(): void {
  }
}

@Directive({
  selector: '[refSearchDisplay]'
})
export class RefSearchDisplay implements OnInit {
  constructor(public parent: RefTableSelectComponent, public el: ElementRef) {
    this.parent.valuechange$.subscribe((a: any) => {
      if (a) {
        this.el.nativeElement.value = a.description;
      }
    })

  }

  ngOnInit(): void {
  }
}

@Directive({
  selector: '[refSearchButton]'
})
export class RefSearchButton implements OnInit {
  constructor(private  dialogservice: DialogService
    , public dialogref: MatDialogRef<RefTableSelectComponent>
    , public parent: RefTableSelectComponent) {
  }

  @HostListener('click') onClick() {
    //alert('hi');
    this.dialogservice.open(FrmSelectRefComponent, {
      data: {
        table: this.parent.tablename,
        getList$: this.parent.getList$()
      }
    }).subscribe(res => {
      //alert(res);
      this.parent.valuechange$.next(res);
    })
  }

  ngOnInit(): void {
  }
}

@Component({
  selector: 'ref-table-select',
  templateUrl: './reftableselect.component.html',
  styleUrls: ['./reftableselect.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RefTableSelectComponent),
      multi: true
    }
  ]
})
export class RefTableSelectComponent implements OnInit, ControlValueAccessor {
  @ViewChild('input', {read: RefSearchInput}) input: RefSearchInput;
  @ViewChild('display', {read: RefSearchDisplay}) display: RefSearchDisplay;
  @ViewChild('button', {read: RefSearchButton}) button: RefSearchButton;
  @Input() tablename: string;

  public valuechange$ = new Subject();
  public changed: (value: any) => void;
  public touched: () => void;
  public isDisabled: boolean;
  public tableResult: any = null;
  tabresult :any=[];
  item :any;
  constructor(private componentservice: ComponentService) {

  }

  getList$(keyWord?) {
    return this.componentservice.readrefdata(this.tablename).pipe(
      tap((tableResult): any => {
        this.tableResult = tableResult;
      })
    );
  }
  
  writeValue(value: any): void {
    // we will get config remote or not dynamically
    const isRemote = true;

    if (value && (isRemote || !this.tableResult)) {
      this.getList$(value).subscribe((result) => {
        // find in local
        this.tabresult=result;
        this.item=this.tabresult.filter(t=>t.code.toLowerCase() ===value.toLowerCase())[0];

        if (this.item){
          this.valuechange$.next(this.item);
        }
        else{
          this.valuechange$.next({code:'',description:''});
        }
       

      })
    } else {
      // find in local
      const item = {
        code: 'local',
        description: 'Local',
      };

      this.valuechange$.next(item);
    }
  }

  registerOnChange(fn: any): void {
    this.changed = fn;

  }

  registerOnTouched(fn: any): void {
    this.touched = fn;

  }

  setDisabledState?(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }

  ngOnInit(): void {
    this.valuechange$.subscribe((value) => {
      //this.changed(value);
    })
  }
}


