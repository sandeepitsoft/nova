import { Component, OnChanges,Input,Output,EventEmitter, Optional, Self } from '@angular/core';
import { ControlValueAccessor, FormControl, NgControl } from '@angular/forms';
//import {HttpClient } from '@angular/common/http';
//import { Observable } from 'rxjs';
import { ComponentService } from '../../componentservice.service';
import { SimpleChanges } from '@angular/core';
// we can now access environment.apiUrl
//const API_URL = environment.apiUrl;

@Component({
  selector: 'refdataselect',
  templateUrl: './refdataselect.component.html',
  styleUrls: ['./refdataselect.component.css']
})

export class RefDataSelectComponent implements OnChanges,ControlValueAccessor {
 //The internal data model
 private innerValue: any = '';
 
 public identifier = `form-select-${identifier++}`;
 options: any = [];
 //selectedOption: string;
 //uri="http://localhost:8080"

 //input/output parameters
  @Input() tablename: string;
  @Input() isdisabled:boolean=true;
  @Input() ischild:boolean;
  @Input() filter: string;
  @Input() placeholder: string;
 
  @Output() selectedvalue:EventEmitter<any>=new EventEmitter();
  
  //Placeholders for the callbacks which are later providesd
  //by the Control Value Accessor
  private onTouchedCallback: () => void = noop;
  private onChangeCallback: (_: any) => void = noop;

staticdata: any = [
  {
    code: "cd1",
    description: "Item1"
  },
  {
    code: "cd2",
    description: "Item2"
  },
  {
    code: "cd3",
    description: "Item3"
  }
];


  constructor(private componetservice:ComponentService,@Optional() @Self() public ngControl: NgControl) { 
    if (this.ngControl != null) {
      // Setting the value accessor directly (instead of using
      // the providers) to avoid running into a circular import.
      this.ngControl.valueAccessor = this;
    }
  }
  //get accessor
  get value(): any {
    return this.innerValue;
  };

  //set accessor including call the onchange callback
  set value(v: any) {
      if (v !== this.innerValue) {
          this.innerValue = v;
          this.onChangeCallback(v);
      }
  }
  //Set touched on blur
  onBlur() {
    this.onTouchedCallback();
  }
  writeValue(value: any) {
    if (value !== this.innerValue) {
      this.innerValue = value;
  }
  }
  //From ControlValueAccessor interface
  registerOnChange(fn: any) {
    this.onChangeCallback = fn;
  }
  //From ControlValueAccessor interface
  registerOnTouched(fn: any) {
  this.onTouchedCallback = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
    
  }

  ngOnChanges(changes: SimpleChanges)  {
    
    console.log(this.ischild);
    if (this.ischild)
    {
      if (changes.filter) {
        if(this.filter){
          console.log(this.filter);
          const finalfilter=this.tablename + '&code='+this.filter
         // this.getItems(this.uri +'/SEC310/getrefdata?reftable='+this.tablename + '&code='+this.filter);
         this.getItems(finalfilter);
        }
      }
    }
    else{
     // const finalfilter=this.tablename;
      this.getItems(this.tablename);
     //this.getItems(this.uri +'/SEC310/getrefdata?reftable='+ this.tablename);
    }
 
   // this.options=this.staticdata;
   
  }
  getItems(filter){
    //console.log(uri);
    this.componetservice.readrefdata(filter)
    .subscribe(data =>{
        this.options = data;
        this.isdisabled=false;
    });
  }
  //emit selected value from select field to parent.
  getselected(value){
    this.selectedvalue.emit(value);
  }
}

export class NovaRefData {
  code: string;
  description: string;
}
const noop = () => {
};
let identifier = 0;
