import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RefDataSelectComponent } from "./refdataselect.component";



describe('RefdataselectComponent', () => {
  let component: RefDataSelectComponent;
  let fixture: ComponentFixture<RefDataSelectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RefDataSelectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RefDataSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
