import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TableComponent } from "./table/table.component";
import { TableDirective } from "./table/table.directive";
import {DataPropertyGetterPipe} from './table/data-property-getter-pipe/data-property-getter.pipe';
import {
  RefSearchButton,
  RefSearchDisplay, RefSearchInput,
  RefTableSelectComponent
} from "./formfield/reftableselect/reftableselect.component";
import { RefDataSelectComponent } from "./formfield/refdataselect/refdataselect.component";
import { MaterialModule } from "./material.module";
import { DialogComponent } from './dialog/dialog.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FrmSelectRefComponent } from './formfield/reftableselect/selectdialog/frm-select-ref/frm-select-ref.component';

const dialog= [
  DialogComponent
];
const table = [
  TableComponent,
  TableDirective,
  DataPropertyGetterPipe
];

const formField = [
  RefTableSelectComponent,
  RefSearchButton,
  RefSearchDisplay,
  RefSearchInput,
  RefDataSelectComponent,
  FrmSelectRefComponent
];


@NgModule({
  declarations: [
    ...table,
    ...formField,
    ...dialog
  ],
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule
  ],
  exports: [
    ...table,
    ...formField,
    ...dialog,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [
    { provide: MAT_DIALOG_DATA, useValue: {} },
    { provide: MatDialogRef, useValue: {} }
  ],
})
export class WidgetModule {
}
