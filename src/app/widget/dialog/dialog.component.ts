import {Component, ComponentFactoryResolver, ComponentRef, Inject, OnDestroy, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent implements OnInit,OnDestroy {
  @ViewChild('target', { static: true,read: ViewContainerRef }) viewContainerRef: ViewContainerRef;
  componentRef: ComponentRef<any>;


  constructor(private resolver: ComponentFactoryResolver,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

    ngOnInit() {
      const factory = this.resolver.resolveComponentFactory(this.data.componentToDisplay);
      this.componentRef = this.viewContainerRef.createComponent(factory);
      this.componentRef.instance.isOpenedAsDialog = true;
    }
    ngOnDestroy() {
      if (this.componentRef) {
        this.componentRef.destroy();
      }

}
}
