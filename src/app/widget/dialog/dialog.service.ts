import {Injectable, TemplateRef} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {ComponentType} from '@angular/cdk/portal';
import {DialogComponent} from './dialog.component';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  constructor(public dialog: MatDialog) { }
  private conf = { autoFocus: true };

  private diagConf = {height: 'auto', width: 'auto', ...this.conf};

  public open<T>(componentToDisplay: ComponentType<T> | TemplateRef<T>, data: any):Observable<any>{
    this.diagConf['data'] = { componentToDisplay, data };
    const conf = this.diagConf;
    let dialogref:MatDialogRef<DialogComponent>;
    dialogref= this.dialog.open(DialogComponent, conf);
    return dialogref.afterClosed();
  }

}
