import { Injectable } from '@angular/core';
import {HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ComponentService {
  uri="http://localhost:8080"
  refdatabaseuri="SEC310/getrefdata?reftable"
  constructor(private http:HttpClient) { }

  readrefdata<T>(filter :string) : Observable<T | T[]> {
    return this.http.get<T | T[]>(`${this.uri}/${this.refdatabaseuri}=${filter}`)

    
    /*.pipe(

      retry(2)
    )*/
  }

}
