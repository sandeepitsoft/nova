import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse 
} from '@angular/common/http';
import { Observable,throwError  } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ErrorDialogService } from './error-dialog.service'

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {

  constructor(private ErrorDialogService: ErrorDialogService) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          let errorMsg = '';
          if (error.error instanceof ErrorEvent) {
           // console.log('this is client side error');
          // errorMsg = `Error: ${error.error.message}`;
            this.ErrorDialogService.openDialog(error.message ?? JSON.stringify(error), error.status);
          }
          else {
            //console.log('this is server side error');
            this.ErrorDialogService.openDialog(error.message ?? JSON.stringify(error), error.status);
            
          }
          //console.log(errorMsg);
          return throwError('Something bad happened; please try again later.');
        })
      )
  }
}
