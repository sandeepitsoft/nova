import { NgModule,ErrorHandler } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GlobalErrorHandlerService } from "./global-error-handler.service";
import { HttpErrorInterceptor } from "./http-error.interceptor";
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { ErrorDialogComponent } from './error-dialog/error-dialog.component';
import { ErrorDialogService } from "./error-dialog.service";
import { MaterialModule } from "../material.module";
@NgModule({
  declarations: [ErrorDialogComponent],
  imports: [CommonModule,MaterialModule],
  providers: [
    ErrorDialogService,
    { provide: ErrorHandler, useClass: GlobalErrorHandlerService },
    { provide: HTTP_INTERCEPTORS, useClass: HttpErrorInterceptor, multi: true }
  ]
})
export class ErrorHandlerModule { }
