import { Injectable } from '@angular/core';

/**
 * See doc here https://ionicframework.com/docs/v3/api/util/Events/
 */
@Injectable({
  providedIn: 'root'
})
export class EventService {
  private c: {[topic: string]: Function[]} = [] as any;

  /**
   * Subscribe to an event topic. Events that get posted to that topic will trigger the provided handler.
   *
   * @param topic the topic to subscribe to
   * @param handlers {function} handler the event handler
   */
  subscribe(topic: string | string[], ...handlers: Function[]) {
    console.log('subscribe-', topic);
    if (typeof topic === 'string') {
      this.sub(topic, ...handlers);
    } else if (Array.isArray(topic) && topic.length) {
      topic.forEach(t => {
        this.sub(t, ...handlers);
      });
    }
  }

  private sub(topic: string, ...handlers: Function[]) {
    if (!this.c[topic]) {
      this.c[topic] = [];
    }
    handlers.forEach((handler) => {
      this.c[topic].push(handler);
    });
  }

  /**
   * Unsubscribe from the given topic. Your handler will no longer receive events published to this topic.
   *
   * @param {string} topic the topic to unsubscribe from
   * @param {function} handler the event handler
   *
   * @return true if a handler was removed
   */
  unsubscribe(topic: string, handler: Function = null) {
    console.log('unsubscribe-' + topic);
    const t = this.c[topic];
    if (!t) {
      // Wasn't found, wasn't removed
      return false;
    }

    if (!handler) {
      // Remove all handlers for this topic
      delete this.c[topic];
      return true;
    }

    // We need to find and remove a specific handler
    const i = t.indexOf(handler);

    if (i < 0) {
      // Wasn't found, wasn't removed
      return false;
    }

    t.splice(i, 1);

    // If the channel is empty now, remove it from the channel map
    if (!t.length) {
      delete this.c[topic];
    }

    // console.warn('UNSUBSCRIBE ' + topic);
    return true;
  }

  unsubscribes(topics: any[]) {
    if (topics && topics.length) {
      for (const topic of topics) {
        this.unsubscribe(topic);
      }
    }
  }

  /**
   * Un subscribe all event
   */
  unsubscribeAll() {
    this.c = <any>[];
    return true;
  }

  /**
   * Publish an event to the given topic.
   *
   * @param topic topic the topic to publish to
   * @param args eventData the data to send as the event
   * @returns {any}
   */
  publish(topic: string, ...args: any[]) {
    console.log('publish-' + topic);
    const t = this.c[topic];
    if (!t) {
      return null;
    }

    const responses: any[] = [];
    t.forEach((handler: any) => {
      responses.push(handler(...args));
    });
    return responses;
  }
}
