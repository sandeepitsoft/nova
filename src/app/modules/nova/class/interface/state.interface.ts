import { FormStateManager } from '../state/form-state-manager';
import { BaseState } from '../base/base-state';

export interface State {
  getStateName(): string;

  skipToState(context: FormStateManager, state: typeof BaseState): void;
}
