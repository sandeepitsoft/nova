import { BtnTypeConstant } from '../constants/btn-type.constant';

export interface BtnStateInterface {
  [BtnTypeConstant.SEARCH]?: boolean;
  [BtnTypeConstant.CHANGE]?: boolean;
  [BtnTypeConstant.CLEAR]?: boolean;
  [BtnTypeConstant.SAVE]?: boolean;
  [BtnTypeConstant.CANCEL]?: boolean;
  [BtnTypeConstant.INSERT]?: boolean;
  [BtnTypeConstant.UPDATE]?: boolean;
  [BtnTypeConstant.VIEW]?: boolean;
  [BtnTypeConstant.CLOSE]?: boolean;
}
