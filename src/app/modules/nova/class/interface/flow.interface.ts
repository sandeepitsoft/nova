import { StateTypeConstant } from '../constants/state-type.constant';
import { BtnTypeConstant } from '../constants/btn-type.constant';
import { BaseState } from '../base/base-state';

export interface FlowInterface {
  defined: {
    [key in BtnTypeConstant]?: {
      navigateTo?: typeof BaseState;
      enableIn: Array<StateTypeConstant>
    };
  };
}
