import { FormStateManager } from '../state/form-state-manager';
import { Directive } from '@angular/core';

@Directive()
export class BaseState {
  skipToState(context: FormStateManager, state: any): void {
    context.setState(new state());
  }

  getStateName(): string {
    return this.constructor.name;
  }
}
