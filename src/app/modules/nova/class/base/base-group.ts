import { AfterContentInit, AfterViewInit, ContentChildren, Directive, ElementRef, Input, QueryList } from '@angular/core';
import { InputDirective } from '../../components/group-control/controls/input.directive';
import { RadioDirective } from '../../components/group-control/controls/radio.directive';
import { SelectDirective } from '../../components/group-control/controls/select.directive';
import { CheckboxDirective } from '../../components/group-control/controls/checkbox.directive';
import { FormStateManager } from '../state/form-state-manager';
import { ButtonDirective } from '../../components/group-control/controls/button.directive';
import { TableDirective } from '../../components/group-control/controls/table.directive';

@Directive()
export abstract class BaseGroup implements AfterViewInit, AfterContentInit {
  @ContentChildren(InputDirective, { descendants: true }) fields: QueryList<InputDirective>;
  @ContentChildren(RadioDirective, { descendants: true }) radios: QueryList<RadioDirective>;
  @ContentChildren(SelectDirective, { descendants: true }) selects: QueryList<SelectDirective>;
  @ContentChildren(CheckboxDirective, { descendants: true }) checkboxs: QueryList<CheckboxDirective>;
  @ContentChildren(ButtonDirective, { descendants: true }) buttons: QueryList<ButtonDirective>;
  @ContentChildren(TableDirective, { descendants: true }) tables: QueryList<TableDirective>;

  @Input() stateManager: FormStateManager;

  constructor(protected eleRef: ElementRef) {

  }

  ngOnInit() {

  }

  abstract checkState(): void;

  applyDisableState(disabled): void {
    [
      this.fields,
      this.radios,
      this.selects,
      this.checkboxs,
      this.buttons,
      this.tables,
    ].forEach((controls) => {
      controls.forEach((control) => {
        control.disable(disabled);
      });
    });
  }

  ngAfterViewInit(): void {

  }

  ngAfterContentInit(): void {
    this.stateManager.changeState$.subscribe(() => {
      this.checkState();
    });
  }
}
