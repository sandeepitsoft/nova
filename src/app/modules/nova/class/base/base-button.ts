import { FormStateManager } from '../state/form-state-manager';
import { BtnTypeConstant } from '../constants/btn-type.constant';
import { Directive, HostListener, Input, OnInit } from '@angular/core';
import { FlowInterface } from '../interface/flow.interface';
import { BaseItem } from './base-item';

@Directive()
export abstract class BaseButton extends BaseItem implements OnInit{
  @Input() stateManager: FormStateManager;

  // Wait a promise to be resolved to move next state
  @Input() promise;
  @Input() clickFn;

  type: BtnTypeConstant;
  protected eleRef;
  protected host;
  onClick: () => void = null;

  ngOnInit(): void {
    this.stateManager.changeState$.subscribe(() => {
      this.host.disabled = this.disableLogic(this.stateManager, this.type);
    });
  }

  @HostListener('click') click(): void {
    if (this.promise) {
      // this.handle();
    } else if (this.onClick) {
      this.onClick();
    } else {
      this.handleTransition();
    }
  }

  protected handleTransition(): void {
    const currentState = this.stateManager.getCurrentState();
    const flow: FlowInterface = this.stateManager.getFlow();
    const navigateToState = flow.defined[this.type].navigateTo;

    currentState.skipToState(this.stateManager, navigateToState);

    if (this.clickFn) {
      this.clickFn();
    }

    if (this.extraHandle) {
      this.extraHandle();
    }
  }

  protected extraHandle(): void {
    // override here
  }

  protected disableLogic(stateManager: FormStateManager, type): boolean {
    const currentState = this.stateManager.getCurrentState();
    const btnLogic = stateManager.getFlow()?.defined[type];

    if (btnLogic?.enableIn?.length) {
      return !btnLogic.enableIn.includes(currentState.getStateName());
    }

    return false;
  }
}
