import { Router } from '@angular/router';
import { MatDialogRef } from '@angular/material/dialog';
import { AfterViewInit, Directive, ViewChild } from '@angular/core';
import { DetailGroupDirective } from '../../components/group-control/detail-group.directive';
import { FormGroup, FormControl, FormBuilder, FormArray } from '@angular/forms';

@Directive()
export abstract class BaseFormComponent implements AfterViewInit{
  abstract stateContext;
  isOpenedAsDialog: boolean;
  dialogref: MatDialogRef<any>;
  router: Router;
  form;
  structure;

  trackArrayProperty = {};

  @ViewChild(DetailGroupDirective, {read: DetailGroupDirective}) detailSection;

  constructor(public fb?: FormBuilder) {
  }

  ngOnInit(): void {
    if (this.structure) {
      this.form = this.buildReactiveControl(null, this.structure);

      this.form.patchValue({
        txtMktCde: 'ddd',
        chkSclBas2: true,
        fraDbtCdt: 'chkDbt',
        first_name: 'Jane',
        last_name: 'Doe',
        address: {
          street_1: '123 Main St.',
          city: 'Las Vegas',
          state: 'NV',
          zip_code: '89123'
        },
        birthday: '1999-09-21',
        phone_numbers: [
          { type: 'cell', number: '702-123-4567' },
          { type: 'work', number: '702-987-6543' }
        ],
        notes: '(This is an example of an uninteresting note.)'
      });
    }

    console.log(this);
  }

  selectedRow($event): void {

  }

  onClose(): void {
    if (this.isOpenedAsDialog) {
      this.dialogref.close();
    } else {
      this.router.navigate(['/']);
    }
  }

  buildReactiveControl(key, item): any {
    let data;

    if (item.type === 'string' || item.type === 'boolean') {
      data = new FormControl('');
    } else if (item.type === 'object') {
      const tmpObj = {};

      for (const prop in item.properties) {
        if (item.properties.hasOwnProperty(prop)) {
          tmpObj[prop] = this.buildReactiveControl(prop, item.properties[prop]);
        }
      }

      data = tmpObj;
    } else if (item.type === 'array') {
      const formArray: FormArray = this.fb.array([]);

      // we track this group and call this when we want to add this group append into formArray
      this.trackArrayProperty[key] = (value) => {
        const group: FormGroup = this.buildReactiveControl(null, item.items);

        if (value) {
          group.patchValue(value);
        }

        formArray.push(group);
      };

      data = formArray;
    }

    if (!key) {
      return this.fb.group(data);
    } else {
      return data;
    }
  }

  ngAfterViewInit(): void {

  }
}
