import { Directive, Input, OnDestroy } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { flatMap } from 'rxjs/internal/operators';
import { of } from 'rxjs';
import { EventService } from '../../services/event.service';
import { AppInjector } from '../../../../app.component';

@Directive()
export abstract class BaseItem implements OnDestroy{
  protected eventService: EventService;
  protected itemName = '';

  constructor() {
    this.eventService = AppInjector.get(EventService);
  }

  @Input() set controlName(name) {
    this.itemName = name;

    this.eventService.subscribe(`${this.itemName}-finally`, () => {
      this.finallyCallback();
    });

    this.eventService.subscribe(`${this.itemName}-complete`, (data) => {
      this.completeCallback(data);
    });

    this.eventService.subscribe(`${this.itemName}-error`, (err) => {
      this.errorCallback(err);
    });

    this.onSetName();
  }

  triggerEvent() {
    this.eventService.publish(this.itemName);
  }

  registerEvent(observable$) {
    this.eventService.subscribe(this.itemName, () => {
      this.requestAsync(observable$);
    });
  }

  private requestAsync(observable): void {
    const source = of(1);

    source.pipe(
      flatMap(() => {
        return observable
      }),
      // flatMap(() => {
      //   return throwError('This is an error!');
      // }),
      finalize(() => {
        this.eventService.publish(`${this.itemName}-finally`);
      }),
    ).subscribe(
      (data) => {
        this.eventService.publish(`${this.itemName}-complete`, data);
      },
      (err) => {
        this.eventService.publish(`${this.itemName}-error`, err);
      });
  }

  completeCallback(data): void {
    // for override
  }

  finallyCallback(): void {
    // for override
  }

  errorCallback(err): void {
    // for override
  }

  onSetName(): void {
    // for override
  }

  ngOnDestroy() {
    this.eventService.unsubscribes([
      `${this.itemName}-finally`,
      `${this.itemName}-complete`,
      `${this.itemName}-error`,
    ])
  }
}
