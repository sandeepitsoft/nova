export enum StateTypeConstant {
  SEARCH = 'SearchState',
  GRID = 'GridState',
  DETAIL = 'DetailState',
}
