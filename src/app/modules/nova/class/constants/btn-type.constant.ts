export enum BtnTypeConstant {
  SEARCH = 'search',
  CHANGE = 'change',
  CLEAR = 'clear',
  SAVE = 'save',
  CANCEL = 'cancel',
  INSERT = 'insert',
  UPDATE = 'update',
  VIEW = 'view',
  CLOSE = 'close',
  OK = 'ok',
  CUSTOM = 'custom',
}
