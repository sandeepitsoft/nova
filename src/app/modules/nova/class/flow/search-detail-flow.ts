import { BtnTypeConstant } from '../constants/btn-type.constant';
import { StateTypeConstant } from '../constants/state-type.constant';
import { FlowInterface } from '../interface/flow.interface';
import { BaseFlow } from '../base/base-flow';
import { GridState } from '../state/grid-state';
import { SearchState } from '../state/search-state';
import { DetailState } from '../state/detail-state';

export class SearchDetailFlow extends BaseFlow implements FlowInterface{
  public defined = {
    [BtnTypeConstant.SEARCH]: {
      navigateTo: GridState,
      enableIn: [StateTypeConstant.SEARCH]
    },
    [BtnTypeConstant.CLEAR]: {
      navigateTo: SearchState,
      enableIn: [StateTypeConstant.GRID]
    },
    [BtnTypeConstant.INSERT]: {
      navigateTo: DetailState,
      enableIn: [StateTypeConstant.SEARCH, StateTypeConstant.GRID]
    },
    [BtnTypeConstant.UPDATE]: {
      navigateTo: DetailState,
      enableIn: [StateTypeConstant.GRID]
    },
    [BtnTypeConstant.CLOSE]: {
      enableIn: [StateTypeConstant.SEARCH, StateTypeConstant.GRID, StateTypeConstant.DETAIL]
    },

    [BtnTypeConstant.SAVE]: {
      navigateTo: GridState,
      enableIn: [StateTypeConstant.DETAIL]
    },
    [BtnTypeConstant.CANCEL]: {
      navigateTo: GridState,
      enableIn: [StateTypeConstant.DETAIL]
    }
  };
}
