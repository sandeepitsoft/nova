import { BtnTypeConstant } from '../constants/btn-type.constant';
import { StateTypeConstant } from '../constants/state-type.constant';
import { FlowInterface } from '../interface/flow.interface';
import { BaseFlow } from '../base/base-flow';
import { GridState } from '../state/grid-state';
import { DetailState } from '../state/detail-state';

export class SimpleDetailFlow extends BaseFlow implements FlowInterface{
  public defined = {
    [BtnTypeConstant.INSERT]: {
      navigateTo: DetailState,
      enableIn: [StateTypeConstant.GRID]
    },
    [BtnTypeConstant.UPDATE]: {
      navigateTo: DetailState,
      enableIn: [StateTypeConstant.GRID]
    },
    [BtnTypeConstant.SAVE]: {
      navigateTo: DetailState,
      enableIn: [StateTypeConstant.GRID]
    },
    [BtnTypeConstant.CLOSE]: {
      enableIn: [StateTypeConstant.GRID]
    },


    [BtnTypeConstant.OK]: {
      enableIn: [StateTypeConstant.DETAIL],
      navigateTo: GridState,
    },
    [BtnTypeConstant.CANCEL]: {
      navigateTo: GridState,
      enableIn: [StateTypeConstant.DETAIL]
    }
  };
}
