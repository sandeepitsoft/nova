import { State } from '../interface/state.interface';
import { SearchState } from './search-state';
import { ReplaySubject } from 'rxjs';
import { BtnTypeConstant } from '../constants/btn-type.constant';
import { FlowInterface } from '../interface/flow.interface';

export class FormStateManager {
  private state: State;
  public changeState$ = new ReplaySubject();
  private defaultStateBtn = {};

  private _initState: State = null;
  private _flow: FlowInterface = null;

  constructor(flow: FlowInterface, defaultState?) {
    const state = defaultState ?? new SearchState();

    this._initState = state;
    this._flow = flow;
    for (const name in BtnTypeConstant) {
      if (BtnTypeConstant.hasOwnProperty(name)) {
        this.defaultStateBtn[BtnTypeConstant[name]] = false;
      }
    }

    this.setState(state);
  }

  /**
   * Set the current state.
   * Normally only called by classes implementing the State interface.
   * @param newState the new state of this context
   */
  setState(newState: State): void {
    this.state = newState;

    this.changeState$.next();
  }

  getCurrentState(): State {
    return this.state;
  }

  getInitState(): State {
    return this._initState;
  }

  getFlow(): FlowInterface {
    return this._flow;
  }
}
