import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BtnCancelDirective } from './components/buttons/transition/btn-cancel.directive';
import { BtnChangeDirective } from './components/buttons/transition/btn-change.directive';
import { BtnSearchDirective } from './components/buttons/transition/btn-search.directive';
import { BtnUpdateDirective } from './components/buttons/transition/btn-update.directive';
import { BtnCloseDirective } from './components/buttons/transition/btn-close.directive';
import { BtnOkDirective } from './components/buttons/transition/btn-ok.directive';
import { BtnSaveDirective } from './components/buttons/transition/btn-save.directive';
import { DetailGroupDirective } from './components/group-control/detail-group.directive';
import { BtnInsertDirective } from './components/buttons/transition/btn-insert.directive';
import { BtnClearDirective } from './components/buttons/transition/btn-clear.directive';
import { InputDirective } from './components/group-control/controls/input.directive';
import { RadioDirective } from './components/group-control/controls/radio.directive';
import { SelectDirective } from './components/group-control/controls/select.directive';
import { CheckboxDirective } from './components/group-control/controls/checkbox.directive';
import { SearchGroupDirective } from './components/group-control/search-group.directive';
import { ButtonDirective } from './components/group-control/controls/button.directive';
import { GridGroupDirective } from './components/group-control/grid-group.directive';
import { TableDirective } from './components/group-control/controls/table.directive';
import { MaterialModule } from "../../widget/material.module";
import { BtnCustomDirective } from "./components/buttons/transition/btn-custom.directive";


const transitionDirectives = [
  BtnCancelDirective,
  BtnChangeDirective,
  BtnSearchDirective,
  BtnInsertDirective,
  BtnUpdateDirective,
  BtnCloseDirective,
  BtnOkDirective,
  BtnSaveDirective,
  BtnClearDirective,
  BtnCustomDirective
];

const groupDirective = [
  DetailGroupDirective,
  SearchGroupDirective,
  GridGroupDirective,

  InputDirective,
  RadioDirective,
  SelectDirective,
  CheckboxDirective,
  ButtonDirective,
  TableDirective
];

@NgModule({
  declarations: [
    ...transitionDirectives,
    ...groupDirective
  ],
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule
  ],
  exports: [
    ...transitionDirectives,
    ...groupDirective,
    ReactiveFormsModule
  ]
})
export class NovaModule { }
