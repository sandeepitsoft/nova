import { Directive, ElementRef } from '@angular/core';
import { RefDataSelectComponent } from "../../../../../widget/formfield/refdataselect/refdataselect.component";

@Directive({
  selector: '[select]',
})
export class SelectDirective {

  constructor(private ele: ElementRef, private host: RefDataSelectComponent) {
  }

  disable(isDisabled): void {
    this.host.isdisabled = isDisabled;
  }
}
