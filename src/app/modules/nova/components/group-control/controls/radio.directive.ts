import { Directive, ElementRef } from '@angular/core';
import { MatRadioGroup } from '@angular/material/radio';

@Directive({
  selector: '[radio]',
})
export class RadioDirective {

  constructor(private ele: ElementRef, private host: MatRadioGroup) {
  }

  disable(isDisabled): void {
    this.host.disabled = isDisabled;
  }
}
