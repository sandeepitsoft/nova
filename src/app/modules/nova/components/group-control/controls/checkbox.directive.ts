import { Directive, ElementRef } from '@angular/core';
import { MatCheckbox } from '@angular/material/checkbox';

@Directive({
  selector: '[checkbox]',
})
export class CheckboxDirective {

  constructor(private ele: ElementRef, private host: MatCheckbox) {
  }

  disable(isDisabled): void {
    this.host.disabled = isDisabled;
  }
}
