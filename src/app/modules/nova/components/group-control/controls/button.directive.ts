import { Directive, ElementRef } from '@angular/core';
import { MatButton } from '@angular/material/button';

@Directive({
  selector: '[button]',
})
export class ButtonDirective {

  constructor(private ele: ElementRef, private host: MatButton) {
  }

  disable(isDisabled): void {
    this.host.disabled = isDisabled;
  }
}
