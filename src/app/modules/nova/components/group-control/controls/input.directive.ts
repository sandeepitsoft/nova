import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[input]',
})
export class InputDirective {

  constructor(private ele: ElementRef) {
  }

  disable(isDisabled): void {
    this.ele.nativeElement.disabled = isDisabled;
  }
}
