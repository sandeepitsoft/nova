import { Directive, ElementRef } from '@angular/core';
import { BaseGroup } from '../../class/base/base-group';
import { StateTypeConstant } from '../../class/constants/state-type.constant';

@Directive({
  selector: '[searchGroup]',
})
export class SearchGroupDirective extends BaseGroup {
  constructor(protected eleRef: ElementRef) {
    super(eleRef);
  }

  checkState(): void {
    if (this.stateManager.getCurrentState().getStateName() !== StateTypeConstant.SEARCH) {
      this.applyDisableState(true);
    } else {
      this.applyDisableState(false);
    }
  }
}
