import { Directive, ElementRef } from '@angular/core';
import { BaseGroup } from '../../class/base/base-group';
import { StateTypeConstant } from '../../class/constants/state-type.constant';

@Directive({
  selector: '[gridGroup]',
})
export class GridGroupDirective extends BaseGroup {
  constructor(protected eleRef: ElementRef) {
    super(eleRef);
  }

  checkState(): void {
    if (this.stateManager.getCurrentState().getStateName() !== StateTypeConstant.GRID) {
      this.applyDisableState(true);
    } else {
      this.applyDisableState(false);
    }
  }
}
