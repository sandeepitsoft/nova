import { Directive, ElementRef } from '@angular/core';
import { BtnTypeConstant } from '../../../class/constants/btn-type.constant';
import { BaseButton } from '../../../class/base/base-button';
import { MatButton } from '@angular/material/button';

@Directive({
  selector: '[btnSearch]',
})
export class BtnSearchDirective extends BaseButton {
  constructor(protected eleRef: ElementRef, protected host: MatButton) {
    super();
  }

  type = BtnTypeConstant.SEARCH;

  onClick = () => {
    this.host.disabled = true;
    this.triggerEvent();
  };

  finallyCallback(): void {
    this.host.disabled = false;
  }

  completeCallback(): void {
    this.handleTransition();
  }
}
