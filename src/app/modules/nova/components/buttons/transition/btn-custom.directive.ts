import { Directive, ElementRef } from '@angular/core';
import { BtnTypeConstant } from '../../../class/constants/btn-type.constant';
import { BaseButton } from '../../../class/base/base-button';
import { MatButton } from '@angular/material/button';
import { of } from "rxjs";
import { delay } from "rxjs/operators";

@Directive({
  selector: '[btnCustom]',
})
export class BtnCustomDirective extends BaseButton {
  constructor(protected eleRef: ElementRef, protected host: MatButton) {
    super();
  }

  type = BtnTypeConstant.CUSTOM;

  onClick = () => {
    this.host.disabled = true;
    this.triggerEvent();
  };

  onSetName(): void {
    this.registerEvent(of(1).pipe(delay(2000)));
  }

  finallyCallback() {
    this.host.disabled = false;
  }

  completeCallback() {
    alert('1111111');
  }
}
