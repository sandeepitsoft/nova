import { Directive, ElementRef } from '@angular/core';
import { BtnTypeConstant } from '../../../class/constants/btn-type.constant';
import { BaseButton } from '../../../class/base/base-button';
import { MatButton } from '@angular/material/button';

@Directive({
  selector: '[btnInsert]',
})
export class BtnInsertDirective extends BaseButton {

  constructor(protected eleRef: ElementRef, protected host: MatButton) {
    super();
  }

  type = BtnTypeConstant.INSERT;
}
