import { Directive, ElementRef } from '@angular/core';
import { BtnTypeConstant } from '../../../class/constants/btn-type.constant';
import { BaseButton } from '../../../class/base/base-button';
import { MatButton } from '@angular/material/button';

@Directive({
  selector: '[btnClear]',
})
export class BtnClearDirective extends BaseButton {

  constructor(protected eleRef: ElementRef, protected host: MatButton) {
    super();
  }

  type = BtnTypeConstant.CLEAR;
}
